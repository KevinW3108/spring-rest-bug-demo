package de.wokeri.springrestbugdemo.secondary;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@Document
public class Order {
	
	@Id
	private String id;
	
	private String customer;
	
	private DeliveryTerms deliveryTerms;
	
	public Order(String customer, DeliveryTerms deliveryTerms) {
		this.customer = customer;
		this.deliveryTerms = deliveryTerms;
	}
}

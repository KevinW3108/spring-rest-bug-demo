package de.wokeri.springrestbugdemo.secondary;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface OrderRepo extends MongoRepository<Order, String> {
}

package de.wokeri.springrestbugdemo.primary;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface DeliveryTermRepo extends MongoRepository<DeliveryTerm, String> {
}

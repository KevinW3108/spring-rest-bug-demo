package de.wokeri.springrestbugdemo.primary;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@Document
public class DeliveryTerm {
	
	@Id
	private String id;
	
	private String code;
	
	public DeliveryTerm(String code) {
		this.code = code;
	}
}

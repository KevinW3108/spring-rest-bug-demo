package de.wokeri.springrestbugdemo;

import de.wokeri.springrestbugdemo.primary.DeliveryTerm;
import de.wokeri.springrestbugdemo.secondary.DeliveryTerms;
import de.wokeri.springrestbugdemo.secondary.Order;
import de.wokeri.springrestbugdemo.secondary.OrderRepo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.data.rest.core.mapping.ResourceMappings;
import org.springframework.data.rest.core.mapping.ResourceMetadata;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class BugTest3 {
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Autowired
	private ResourceMappings resourceMappings;
	
	@Autowired
	private OrderRepo orderRepo;
	
	@BeforeEach
	public void setUp() {
		DeliveryTerms deliveryTerms = new DeliveryTerms("EXW");
		orderRepo.save(new Order("Kunde", deliveryTerms));
	}
	
	@RepeatedTest(10)
	public void test() {
		// Find first metadata for path /deliveryTerms
		Optional<ResourceMetadata> metadata = resourceMappings.stream()
			.filter(m -> m.getPath().matches("/deliveryTerms"))
			.findFirst();
		
		// Expect metadata to be present
		assertTrue(metadata.isPresent());
		// Expect metadata domain type to be DeliveryTerm.class
		assertEquals(DeliveryTerm.class, metadata.get().getDomainType());
		// Expect metadata to be exported
		assertTrue(metadata.get().isExported());
		
		// Call orders endpoint
		restTemplate.getForEntity("http://localhost:" + port + "/orders", String.class);
		
		// Repeat the test, expect the same as above
		metadata = resourceMappings.stream()
			.filter(m -> m.getPath().matches("/deliveryTerms"))
			.findFirst();
		
		assertTrue(metadata.isPresent());
		assertEquals(DeliveryTerm.class, metadata.get().getDomainType());
		assertTrue(metadata.get().isExported());
	}
}

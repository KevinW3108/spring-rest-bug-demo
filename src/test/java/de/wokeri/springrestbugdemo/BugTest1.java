package de.wokeri.springrestbugdemo;

import de.wokeri.springrestbugdemo.secondary.DeliveryTerms;
import de.wokeri.springrestbugdemo.secondary.Order;
import de.wokeri.springrestbugdemo.secondary.OrderRepo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class BugTest1 {
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Autowired
	private OrderRepo orderRepo;
	
	@BeforeEach
	public void setUp() {
		DeliveryTerms deliveryTerms = new DeliveryTerms("EXW");
		orderRepo.save(new Order("Kunde", deliveryTerms));
	}
	
	@RepeatedTest(10)
	public void test() {
		// Test deliveryTerms endpoint
		ResponseEntity<Object> entity =
			restTemplate.getForEntity("http://localhost:" + port + "/deliveryTerms", Object.class);
		
		// Expect status 200 OK
		assertEquals(HttpStatus.OK, entity.getStatusCode());
		
		// Call orders endpoint
		restTemplate.getForEntity("http://localhost:" + port + "/orders", String.class);
		
		// Repeat the test, expect the same as above
		entity =
			restTemplate.getForEntity("http://localhost:" + port + "/deliveryTerms", Object.class);
		
		assertEquals(HttpStatus.OK, entity.getStatusCode());
	}
}
